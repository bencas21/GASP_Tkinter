API Level Tests for GASP
========================

The function `begin_graphics()` opens a graphics window with a canvas inside
on which our graphics objects will be drawn. Since we only want to test
the API functionality, we will mock all calls to the tkinter backend.

  >>> from gasp import * 
  >>> from tests import mock_tkinter
  >>> gasp.tkinter = mock_tkinter

Now call `begin_graphics()` and `end_graphics()`, confirming that there are no
errors, and no graphics.

  >>> begin_graphics()
  >>> end_graphics()

The size, title, and background color of the GASP window can be set.

  >>> begin_graphics(
  ...    width=800, height=600, title="Robots", background=color.YELLOW
  ... )

The `clear_screen()` function removes all objects from the GASP window.

  >>> clear_screen()

Individual objects can be removed with `remove_from_screen(obj)`, but we need
to create an object before we can test that.

The simplest object is a `Plot`, which puts a single point on the screen where
directed, with color and size also optinally specified.

  >>> Plot((320, 240), color=color.RED, size=3)
  <Plot object at (320, 240)>

  `Line` gives us a line segment between to pairs of coordinates.

  >>> Line((100, 100), (300, 400))
  <Line segment object from (100, 100) to (300, 400)>

`Box` is our next graphical object. First will just place one on the screen.
Next we'll give one a name so we can move and remove it.

  >>> Box((100, 100), 150, 90, color=color.ORANGE, filled=True)
  <Box object 150 wide and 90 high at (100, 100)>
  >>> box = Box((100, 100), 150, 90, color=color.PURPLE, filled=True)
  >>> box.pos
  (100, 100)
  >>> move_by(box, 50, 100)
  >>> box.pos
  (150, 200)

With `move_to` you can move an object to an absolute location.

  >>> move_to(box, (300, 220))
  >>> box.pos
  (300, 220)

The `Circle` object needs a center and a radius. Circles are drawn around their
centers, so aligning them with Boxes takes some adjusting.

  >>> c = Circle((320, 240), 100)
  >>> b = Box((320, 240), 200, 200)

GASP renames the most useful functions in the `random` module to avoid having
to explain how modules work in the beginning process of learning to program.

  >>> n = random_between(5, 10)
  >>> 5 <= n <= 10
  True
  >>> choice = random_choice([2, 4, 6, 8])
  >>> choice in [2, 4, 6, 8]
  True
