# Note: Great reference for Python tkinter:
# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/index.html
import time

from gasp import *

begin_graphics(800, 600)

for y in range(200, 99, -2):
    Plot((402 - y, y), color=color.BLUE)
    time.sleep(0.05)

time.sleep(3)
Line((300, 200), (400, 50), color=color.RED, thickness=3)
time.sleep(3)
Box((50, 400), 100, 150, color=color.PURPLE, filled=True)
time.sleep(3)

# Draw a Circle and Box with the same coordinates and size
c = Circle((320, 240), 100)
b = Box((320, 240), 200, 200)
time.sleep(3)

end_graphics()

#Polygon(((300,300),(350,400),(400,350)), colour=Colour.green,
#                filled=True)
#polygon(((500,300),(550,400),(600,350)), colour=Colour.blue)
#polygon(((510,300),(560,400),(610,350)), colour=Colour.blue, closed=1)
#circle(200,200,40,colour=Colour.red, filled=True)
#circle(200,200,50,colour=Colour.green, endpoints=(10,45))
#circle(200,200,50,colour=Colour.blue,
#endpoints=((195,190), 200), filled=True)
#move(100,100)
#text('Hello world', serifs=1, size=14)
