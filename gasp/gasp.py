#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This program is part of GASP, a toolkit for newbie Python Programmers.
# Copyright 2020 (C) Richard Crook, Gareth McCaughan, and the GASP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Publicly accessable API of GASP.

GASP is designed to be imported with `from gasp import *`.

To use any methods in this application, you must first run `begin_graphics()`.
"""

import random
import tkinter
import time

from . import color


_root_window = None

_canvas = None
_canvas_xs = None
_canvas_ys = None
_canvas_col = None
_canvas_tsize = 12
_canvas_tserifs = 0
_canvas_tfonts = ["times", "lucidasans-24"]

_mouse_enabled = 0
_mouse_x = None
_mouse_y = None

_returning = 0


# === Time support ===========================================================


def sleep(secs):
    if _root_window == None:
        time.sleep(secs)
    else:
        _root_window.update_idletasks()
        _root_window.after(int(1000 * secs), _root_window.quit)
        _root_window.mainloop()


# === Initialization =========================================================


def begin_graphics(width=640, height=480, background=color.WHITE, title="GASP"):
    global _root_window, _canvas, _canvas_xs, _canvas_ys

    # Check for duplicate call
    if _root_window is not None:
        _root_window.destroy()

    # Save the canvas size parameters
    _canvas_xs, _canvas_ys = width - 1, height - 1

    # Create the root window
    _root_window = tkinter.Tk()
    _root_window.protocol("WM_DELETE_WINDOW", _destroy_window)
    _root_window.title(title)
    _root_window.resizable(0, 0)

    # Create the canvas object
    try:
        _canvas = tkinter.Canvas(
            _root_window, width=width, height=height, bg=background
        )
        _canvas.pack()
        _canvas.update()
    except:
        _root_window.destroy()
        _root_window = None

    # Bind to key-down and key-up events
    _root_window.bind("<KeyPress>", _keypress)
    _root_window.bind("<KeyRelease>", _keyrelease)
    _root_window.bind("<FocusIn>", _clear_keys)
    _root_window.bind("<FocusOut>", _clear_keys)
    _clear_keys()


def _destroy_window(event=None):
    global _root_window
    _root_window.destroy()
    _root_window = None


# === End graphics ===========================================================


def end_graphics():
    """
    Program terminated. Wait for graphics window to be closed.
    """
    global _root_window, _canvas, _mouse_enabled
    try:
        sleep(1)
        _root_window.destroy()
    finally:
        _root_window = None
        _canvas = None
        _mouse_enabled = 0
        _clear_keys()


# === Object drawing  ========================================================


class Plot:
    """Plot a point on the screen."""

    def __init__(self, p, color=color.BLACK, size=1):
        self.pos = p
        self.color = color
        self.size = size
        self.coord_list = [
            p[0] - size,
            _canvas_ys - p[1] - size,
            p[0] + size,
            _canvas_ys - p[1] + size,
        ]
        # Draw a small circle
        self.id = _canvas.create_oval(*self.coord_list, outline=color, fill=color)
        _canvas.update()

    def __repr__(self):
        return f"<Plot object at ({self.pos[0]}, {self.pos[1]})>"


class Line:
    """Draw a line segment between p1 and p2."""

    def __init__(self, p1, p2, color=color.BLACK, thickness=1):
        self.pos = p1
        self.rise = p2[0] - p1[0]
        self.run = p2[1] - p1[1]
        self.coord_list = [p1[0], _canvas_ys - p1[1], p2[0], _canvas_ys - p2[1]]

        self.id = _canvas.create_line(*self.coord_list, fill=color, width=thickness)
        _canvas.update()

    def __repr__(self):
        return (
            f"<Line segment object from ({self.pos[0]}, {self.pos[1]})"
            f" to ({self.pos[0] + self.rise}, {self.pos[1] + self.run})>"
        )


class Box:
    """Draw a rectangle given lower right corner and width and height."""

    def __init__(self, corner, width, height, color=color.BLACK, filled=False):
        self.pos = corner
        self.width = width
        self.height = height
        self.color = color
        self.filled = filled
        self.coord_list = [
            corner[0],
            _canvas_ys - corner[1],
            corner[0] + width,
            _canvas_ys - corner[1] - height,
        ]

        fill = color if filled else ""  # transparent

        self.id = _canvas.create_rectangle(*self.coord_list, fill=fill)
        _canvas.update()

    def __repr__(self):
        return (
            f"<Box object {self.width} wide and {self.height} high at "
            f"({self.pos[0]}, {self.pos[1]})>"
        )


class Circle:
    """Draw a circle centered at the given coordinates with the given radius"""

    def __init__(self, center, radius, color=color.BLACK, filled=False):
        self.x = center[0]
        self.y = center[1]
        self.radius = radius
        self.color = color
        self.filled = filled

        x0 = self.x - self.radius
        y0 = self.y - self.radius
        x1 = self.x + self.radius
        y1 = self.y + self.radius

        fill = color if filled else ""

        self.id = _canvas.create_oval(x0, y0, x1, y1, outline=color, fill=fill)
        _canvas.update()

    def __repr__(self):
        return f"<Circle object centered at ({self.position}) with a radius of {self.radius}"


# === Object moving and removing =============================================


def clear_screen(background=None):

    # Remove all drawn items
    _canvas.delete("all")

    # Change background color if requested
    if background is not None:
        _canvas.configure(bg=background)


def remove_from_screen(obj):
    _canvas.delete(obj.id)
    _root_window.tk.dooneevent(tkinter._tkinter.DONT_WAIT)


def move_by(obj, x, y):
    c = obj.coord_list
    # translate each x coordinate right and each y coordinate up
    obj.coord_list = [c[i] + x if i % 2 == 0 else c[i] - y for i in range(len(c))]
    obj.pos = (obj.coord_list[0], _canvas_ys - obj.coord_list[1])
    _canvas.move(obj.id, x, -y)


def move_to(obj, p):
    dx = p[0] - obj.pos[0]
    dy = p[1] - obj.pos[1]
    move_by(obj, dx, dy)


# === Keypress handling  =====================================================

# We bind to key-down and key-up events.
_keysdown = {}
# This holds an unprocessed key release.  We delay key releases by up-to
# one call to keys_pressed() to get round a problem with auto repeat.
_got_release = None


def _keypress(event):
    global _got_release
    _keysdown[event.char] = 1
    _got_release = None


def _keyrelease(event):
    global _got_release
    try:
        del _keysdown[event.char]
    except:
        pass
    _got_release = 1


def _clear_keys(event=None):
    global _keysdown, _got_release
    _keysdown = {}
    _got_release = None


def keys_pressed():
    global _root_window, _got_release
    if _got_release:
        _root_window.tk.dooneevent(tkinter._tkinter.DONT_WAIT)
    return _keysdown.keys()


# Block for a list of keys...
def wait_for_keys():
    keys = []
    while keys == []:
        keys = keys_pressed()
    thekeys = keys
    while keys != []:
        keys = keys_pressed()
    return thekeys


# === Random and read input functions  =======================================

random_choice = random.choice
random_between = random.randint
